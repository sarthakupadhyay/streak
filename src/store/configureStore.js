// @flow
import { createStore, applyMiddleware } from 'redux'
// $FlowFixMe: suppressing this error until we add lib def for this
import thunk from 'redux-thunk'
import rootReducer from '../reducers'

export default function configureStore (initialState: any) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunk)
  )
}
