// @flow
import React, { Component } from 'react'
import './App.css'
import BoxLoader from './Components/BoxLoader'
import Home from './Components/Home'
import { BrowserRouter as Router } from 'react-router-dom'
import { connect } from 'react-redux'
import { authenticate, login } from './actions/auth'

const mapStateToProps = (state) => ({
  isAuthenticated: state.isAuthenticated,
  isLoading: state.isLoading,
  title: state.title
})

const mapDispatchToProps = (dispatch) => ({
  authenticate: () => dispatch(authenticate()),
  login: () => dispatch(login())
})

type Props = {
  isLoading: boolean,
  isAuthenticated: boolean,
  title: string,
  authenticate: () => void,
  login: () => void
};

class App extends Component<Props> {
  componentDidMount () {
    this.props.authenticate()
  }

  componentDidUpdate () {
    document.title = this.props.title
  }

  render () {
    const { isLoading, isAuthenticated } = this.props

    return (
      <Router>
        <div className='App'>
          {isLoading
            ? <BoxLoader />
            : (isAuthenticated
              ? <Home />
              : <a onClick={this.props.login}>login</a>
            )
          }
        </div>
      </Router>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
