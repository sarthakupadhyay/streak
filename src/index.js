// @flow
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import * as firebase from 'firebase'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import { firebaseConfig } from './config/firebase.js'

firebase.initializeApp(firebaseConfig)
const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,
  (document.getElementById('root'): any)
)

registerServiceWorker()
