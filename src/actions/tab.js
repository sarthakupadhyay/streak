// @flow
type Action = {type: string, tabId: number};

export const changeTab = (tabId: number): Action => ({
  type: 'CHANGE_TAB',
  tabId
})

type Dispatch = (action: Action) => any;

export const initTab = (tabName: string) => {
  return (dispatch: Dispatch) => {
    switch (tabName) {
      case 'projects':
        dispatch(changeTab(0))
        break
      case 'tasks':
        dispatch(changeTab(2))
        break
      case 'labels':
        dispatch(changeTab(1))
        break
      case 'inbox':
        dispatch(changeTab(3))
        break
      default:
        dispatch(changeTab(2))
        break
    }
  }
}
