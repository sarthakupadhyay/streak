// @flow
type Action = { type: string, toastId: number}

export const setToastId = (toastId: number) : Action => ({
  type: 'SET_TOAST_ID',
  toastId
})
