// @flow
import * as firebase from 'firebase'
import { isComponentLoading } from './componentLoader.js'
import type { LoadAction } from './componentLoader.js'
import type { Label } from '../Components/Label/Labels.js'
import _ from 'lodash'

type Labels = Array<Label>;

type Action = {
  type: string,
  labels: Labels
};

export const setLabels = (labels: Labels): Action => ({
  type: 'SET_LABELS',
  labels
})

type Dispatch = (action: Action | LoadAction) => any;

const formatLabels = (labels: {} = {}): Labels => (
  _.entries(labels).map(labels => _.set(labels[1], 'id', labels[0]))
)

export const fetchLabels = () => {
  return (dispatch: Dispatch) => {
    dispatch(isComponentLoading(true))

    const uid = firebase.auth().currentUser.uid
    const ref = firebase.database().ref(`/labels/${uid}`)

    ref.on('value', snap => {
      dispatch(isComponentLoading(false))
      if (snap.val()) {
        dispatch(setLabels(formatLabels(snap.val())))
      }
    })
  }
}
