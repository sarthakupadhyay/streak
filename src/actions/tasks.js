// @flow
import * as firebase from 'firebase'
import _ from 'lodash'
import type { LoadAction } from './componentLoader.js'
import type { Task } from '../Components/Task/types/task_types.js'

type Tasks = Array<Task>;

type setTasksAction = {
  type: string,
  tasks: Tasks
};

export const setTasks = (tasks: Tasks): setTasksAction => ({
  type: 'SET_TASKS',
  tasks
})

type Order = 'asc' | 'desc';

type sortTasksAction = {
  type: string,
  key: string,
  order: Order
};

export const sortTasks = (key: string, order: Order): sortTasksAction => ({
  type: 'SORT_TASKS',
  key,
  order
})

type Dispatch = (action?: setTasksAction | LoadAction) => any;
type ThunkAction = (dispatch: Dispatch) => any;

const formatTasks = (tasks: {}): Tasks => (
  _.entries(tasks).map(task => _.set(task[1], 'id', task[0]))
)

const fetchAllTasks = (): ThunkAction => {
  return (dispatch: Dispatch): void => {
    const uid = firebase.auth().currentUser.uid
    const ref = firebase.database().ref(`/tasks/${uid}`)
    ref.on('value', snap => {
      dispatch(setTasks(formatTasks(snap.val())))
    })
  }
}

const fetchInboxTasks = (): ThunkAction => {
  return (dispatch: Dispatch): void => {
    const uid = firebase.auth().currentUser.uid
    const ref = firebase.database().ref(`/inbox/${uid}/tasks`)
    ref.on('value', snap => {
      dispatch(setTasks(formatTasks(snap.val())))
    })
  }
}

const fetchLabelsTasks = (labelId: string): ThunkAction => {
  return (dispatch: Dispatch): void => {
    const uid = firebase.auth().currentUser.uid
    const ref = firebase.database().ref(`/labels/${uid}/${labelId}/tasks`)
    ref.on('value', snap => {
      dispatch(setTasks(formatTasks(snap.val())))
    })
  }
}

const fetchProjectsTasks = (projectId: string): ThunkAction => {
  return (dispatch: Dispatch): void => {
    const uid = firebase.auth().currentUser.uid
    const ref = firebase.database().ref(`/projects/${uid}/${projectId}/tasks`)
    ref.on('value', snap => {
      dispatch(setTasks(formatTasks(snap.val())))
    })
  }
}

type ThunkDispatch = (ThunkAction) => any;
type Opts = { projectId: string, labelId: string };

export const fetchTasks = (filter: string, options: Opts = {projectId: 'null', labelId: 'null'}) => {
  return (dispatch: ThunkDispatch) => {
    switch (filter) {
      case 'INBOX':
        dispatch(fetchInboxTasks())
        break
      case 'ALL':
        dispatch(fetchAllTasks())
        break
      case 'PROJECT':
        dispatch(fetchProjectsTasks(options.projectId))
        break
      case 'LABEL':
        dispatch(fetchLabelsTasks(options.labelId))
        break
      default:
        dispatch(fetchAllTasks())
        break
    }
  }
}

export const stopFetch = (filter: string) => {
  return (dispatch: ThunkDispatch) => {
    const uid = firebase.auth().currentUser.uid
    switch (filter) {
      case 'INBOX':
        firebase.database().ref(`/inbox/${uid}/tasks`).off()
        break
      case 'PROJECT':
        firebase.database().ref(`/projects/${uid}/tasks`).off()
        break
      default:
        firebase.database().ref(`/tasks/${uid}/`).off()
        break
    }
  }
}
