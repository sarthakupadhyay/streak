// @flow
import * as firebase from 'firebase'

type LoadingAction = { type: string, isLoading: boolean};

export const isLoading = (bool: boolean): LoadingAction => ({
  type: 'IS_LOADING',
  isLoading: bool
})

type AuthAction = { type: string, isAuthenticated: boolean};

export const isAuthenticated = (bool: boolean): AuthAction => ({
  type: 'IS_AUTHENTICATED',
  isAuthenticated: bool
})

type Action = LoadingAction | AuthAction;
type Dispatch = (action: Action) => any;

export const authenticate = () => {
  return (dispatch: Dispatch) => {
    dispatch(isLoading(true))
    firebase.auth().getRedirectResult().then(result => {
      dispatch(isLoading(false))
      if (result.credential) {
        // var token = result.credential.accessToken
        dispatch(isAuthenticated(true))
      } else {
        if (firebase.auth().currentUser) {
          dispatch(isAuthenticated(true))
        } else {
          dispatch(isAuthenticated(false))
        }
      }
      // var user = result.user/
    }).catch(error => {
      console.log('Error authenticated!', error)
      dispatch(isLoading(false))
      dispatch(isAuthenticated(false))
    })
  }
}

export const logout = () => {
  return (dispatch: Dispatch) => {
    dispatch(isLoading(true))

    firebase.auth().signOut()
      .then(() => {
        dispatch(isLoading(false))
        dispatch(isAuthenticated(true))
      })
      .catch(error => {
        console.log('Error signing out!', error)
        dispatch(isLoading(false))
        dispatch(isAuthenticated(false))
      })
  }
}

export const login = () => {
  return (dispatch: Dispatch) => {
    dispatch(isLoading(true))
    let provider = new firebase.auth.GoogleAuthProvider()

    provider.addScope('https://www.googleapis.com/auth/userinfo.profile')
    provider.addScope('https://www.googleapis.com/auth/user.birthday.read')
    firebase.auth().getRedirectResult().then(result => {
      if (result.user === null || firebase.auth().currentUser === null) {
        firebase.auth().signInWithRedirect(provider)
      } else {
        dispatch(isLoading(false))
      }
    })
  }
}
