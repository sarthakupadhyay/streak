// @flow
import * as firebase from 'firebase'
import { isComponentLoading } from './componentLoader.js'
import _ from 'lodash'
import type { LoadAction } from './componentLoader.js'
import type { Project } from '../Components/Project/Projects.js'

type Projects = Array<Project>;

type Action = {
  type: string,
  projects: Projects
};

export const setProjects = (projects: Projects = []): Action => ({
  type: 'SET_PROJECTS',
  projects
})

const formatTasks = (projects: {} = {}): Projects => (
  _.entries(projects).map(project => _.set(project[1], 'id', project[0]))
)

type Dispatch = (action: Action | LoadAction) => any;

export const fetchProjects = () => {
  return (dispatch: Dispatch) => {
    dispatch(isComponentLoading(true))

    const uid = firebase.auth().currentUser.uid
    const ref = firebase.database().ref(`/projects/${uid}`)

    ref.on('value', snap => {
      dispatch(isComponentLoading(false))
      dispatch(setProjects(formatTasks(snap.val())))
    })
  }
}
