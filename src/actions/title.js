// @flow
type Action = {type: string, title: string};

export const changeTitle = (title: string): Action => ({
  type: 'CHANGE_TITLE',
  title
})
