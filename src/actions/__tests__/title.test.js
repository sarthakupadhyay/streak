import {changeTitle} from '../title.js'

it('should change title', () => {
  expect(changeTitle('Streaker')).toEqual({
    type: 'CHANGE_TITLE',
    title: 'Streaker'
  })
})
