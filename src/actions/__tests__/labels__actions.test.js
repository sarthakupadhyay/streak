import {setLabels} from '../labels.js'

describe('Label actions', () => {
  const labels = {
    title: 'label1',
    id: 'abc123'
  }

  it('should return action', () => {
    expect(setLabels(labels)).toEqual({
      type: 'SET_LABELS',
      labels: labels
    })
  })
})
