import {setTasks} from '../tasks.js'

describe('Task actions', () => {
  const tasks = {
    title: 'Task1',
    created_on: 12345,
    id: 'abc123',
    project: {
      id: 'abc12',
      title: 'project1',
      color: '#fff'
    }
  }
  it('should return action', () => {
    expect(setTasks(tasks)).toEqual({
      type: 'SET_TASKS',
      tasks: tasks
    })
  })
})
