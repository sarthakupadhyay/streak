import {isLoading, isAuthenticated} from '../auth.js'

describe('Auth actions', () => {
  it('should change isLoading status', () => {
    expect(isLoading(false)).toEqual({
      type: 'IS_LOADING',
      isLoading: false
    })
  })
  it('should change isAuthenticated status', () => {
    expect(isAuthenticated(false)).toEqual({
      type: 'IS_AUTHENTICATED',
      isAuthenticated: false
    })
  })
})
