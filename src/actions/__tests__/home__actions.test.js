import {setToastId} from '../home'

it('should return an action', () => {
  expect(setToastId(1)).toEqual({
    type: 'SET_TOAST_ID',
    toastId: 1
  })
})
