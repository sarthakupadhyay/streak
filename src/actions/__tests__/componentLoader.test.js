import {isComponentLoading} from '../componentLoader.js'

it('should change loading status', () => {
  expect(isComponentLoading(false)).toEqual({
    type: 'IS_COMPONENT_LOADING',
    isComponentLoading: false
  })
})
