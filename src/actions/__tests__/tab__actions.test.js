import {changeTab} from '../tab.js'

it('changes tabId', () => {
  expect(changeTab(123)).toEqual({
    type: 'CHANGE_TAB',
    tabId: 123
  })
})
