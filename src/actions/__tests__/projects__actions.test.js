import {setProjects} from '../projects.js'

describe('Project actions', () => {
  const projects = {
    id: 'abc12',
    title: 'project1',
    color: '#fff'
  }

  it('should return action', () => {
    expect(setProjects(projects)).toEqual({
      type: 'SET_PROJECTS',
      projects: projects
    })
  })
})
