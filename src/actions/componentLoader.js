// @flow
export type LoadAction = { type: string, isComponentLoading: boolean };

export const isComponentLoading = (isComponentLoading: boolean): LoadAction => ({
  type: 'IS_COMPONENT_LOADING',
  isComponentLoading
})
