// @flow
type LoadState = boolean;
type LoadAction = { type: string, isLoading: boolean};

export const isLoading = (state:LoadState = false, action: LoadAction): LoadState => {
  switch (action.type) {
    case 'IS_LOADING':
      return action.isLoading
    default:
      return state
  }
}

type AuthState = boolean;
type AuthAction = { type: string, isAuthenticated: boolean};

export const isAuthenticated = (state:AuthState = false, action: AuthAction): AuthState => {
  switch (action.type) {
    case 'IS_AUTHENTICATED':
      return action.isAuthenticated
    default:
      return state
  }
}
