// @flow
type Action = { type: string, isComponentLoading: boolean };
type State = boolean;

export const isComponentLoading = (state: State = false, action: Action): State => {
  switch (action.type) {
    case 'IS_COMPONENT_LOADING':
      return action.isComponentLoading
    default:
      return state
  }
}
