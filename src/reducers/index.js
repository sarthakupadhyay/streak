// @flow
import { combineReducers } from 'redux'
import { isLoading, isAuthenticated } from './auth'
import { isComponentLoading } from './isComponentLoading.js'
import { title } from './title'
import { tab } from './tab'
import { projects } from './projects'
import { labels } from './labels'
import { tasks } from './tasks'

export default combineReducers({
  isLoading,
  isAuthenticated,
  title,
  tab,
  projects,
  labels,
  tasks,
  isComponentLoading
})
