// @flow
type State = string;
type Action = {type: string, title: string};

export const title = (state:State = 'Streaker', action: Action): State => {
  switch (action.type) {
    case 'CHANGE_TITLE':
      return action.title
    default:
      return state
  }
}
