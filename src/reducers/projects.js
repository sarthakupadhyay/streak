// @flow
import type { Project } from '../Components/Project/Projects.js'

type Projects = Array<Project>;

type State = Projects;

type Action = {
  type: string,
  projects: Projects
};

export const projects = (state:State = [], action:Action): State => {
  // console.log(action)
  switch (action.type) {
    case 'SET_PROJECTS':
      return action.projects
    default:
      return state
  }
}
