// @flow
import type { Label } from '../Components/Label/Labels.js'

type State = Array<Label>;
type Action = {
  type: string,
  labels: Array<Label>
};

export const labels = (state:State = [], action:Action) => {
  switch (action.type) {
    case 'SET_LABELS':
      return action.labels
    default:
      return state
  }
}
