// @flow
import type {Task} from '../Components/Task/types/task_types.js'
import _ from 'lodash'

type State = Array<Task>;

type Order = 'asc' | 'desc';

type Action =
  | { type:'SET_TASKS', tasks: Array<Task> }
  | { type:'SORT_TASKS', key: string, order: Order };

export const tasks = (state:State = [], action:Action): State => {
  switch (action.type) {
    case 'SET_TASKS':
      return action.tasks
    case 'SORT_TASKS':
      return _.orderBy(state, [action.key], [action.order])
    default:
      return state
  }
}
