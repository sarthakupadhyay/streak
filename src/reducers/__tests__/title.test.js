import { title } from '../title.js'

describe('Title Reducer', () => {
  it('should have a default state', () => {
    expect(title(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })

  it('should handle CHANGE_TITLE', () => {
    expect(title(undefined, {
      type: 'CHANGE_TITLE',
      title: 'Tasks'
    })).toMatchSnapshot()
  })
})
