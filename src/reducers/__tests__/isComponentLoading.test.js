
import {isComponentLoading} from '../isComponentLoading.js'

describe('isComponentLoading reducer', () => {
  it('should have a default state', () => {
    expect(isComponentLoading(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })

  it('should handle IS_COMPONENT_LOADING', () => {
    expect(isComponentLoading(undefined, {
      type: 'IS_COMPONENT_LOADING',
      isComponentLoading: false
    })).toMatchSnapshot()
  })
})
