import { labels } from '../labels.js'

describe('Label Reducer', () => {
  it('should have a default state', () => {
    expect(labels(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })

  it('should handle SET_LABELS', () => {
    expect(labels(undefined, {
      type: 'SET_LABELS',
      labels: [{
        title: 'label title',
        id: '1209fcwskmq',
        tasks: [{ title: 'task title', id: '12412r12' }]
      }]
    })).toMatchSnapshot()
  })
})
