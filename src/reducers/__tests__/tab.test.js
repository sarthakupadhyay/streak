import { tab } from '../tab.js'

const randRange = (low, high) => Math.random() * (high - low) + low

describe('Tab Reducer', () => {
  it('should have a default state', () => {
    expect(tab(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })

  it('should handle CHANGE_TAB', () => {
    expect(tab(undefined, {
      type: 'CHANGE_TAB',
      tabId: 4
    })).toMatchSnapshot()
  })

  it('should handle CHANGE_TAB when tabId greater than 4', () => {
    expect(tab(undefined, {
      type: 'CHANGE_TAB',
      tabId: randRange(5, 124982)
    })).toMatchSnapshot()
  })

  it('should handle CHANGE_TAB when tabId less than 0', () => {
    expect(tab(undefined, {
      type: 'CHANGE_TAB',
      tabId: randRange(-514124, -1)
    })).toMatchSnapshot()
  })
})
