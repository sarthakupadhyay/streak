import {projects} from '../projects.js'

describe('Projects reducer', () => {
  it('should have a default state', () => {
    expect(projects(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })

  it('should handle SET_PROJECTS', () => {
    expect(projects(undefined, {
      type: 'SET_PROJECTS',
      projects: {
        id: 'abc1',
        title: 'project1',
        color: '#000'
      }
    })).toMatchSnapshot()
  })
})
