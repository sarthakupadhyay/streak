import {tasks} from '../tasks.js'

describe('Tasks reducer ', () => {
  it('should have a default state', () => {
    expect(tasks(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })

  it('should handle SET_TASKS', () => {
    expect(tasks(undefined, {
      type: 'SET_TASKS',
      tasks: [{
        title: 'Task title',
        created_on: 23458901,
        id: 'abc123',
        project: {
          id: 'projectid',
          title: 'project1',
          color: '#000',
        },
      }]
    })).toMatchSnapshot()
  })
})
