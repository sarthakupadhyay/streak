import { isLoading, isAuthenticated } from '../auth'

describe('Auth Reducer (isLoading)', () => {
  it('should have a default state', () => {
    expect(isLoading(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })
  it('should handle IS_LOADING true', () => {
    expect(isLoading(undefined, {
      type: 'IS_LOADING',
      isLoading: true
    })).toMatchSnapshot()
  })

  it('should handle IS_LOADING false', () => {
    expect(isLoading(undefined, {
      type: 'IS_LOADING',
      isLoading: false
    })).toMatchSnapshot()
  })
})

describe('Auth Reducer (isAuthenticated)', () => {
  it('should have a default state', () => {
    expect(isAuthenticated(undefined, {
      type: 'undefined'
    })).toMatchSnapshot()
  })

  it('should handle IS_AUTHENTICATED true', () => {
    expect(isAuthenticated(undefined, {
      type: 'IS_AUTHENTICATED',
      isAuthenticated: true
    })).toMatchSnapshot()
  })

  it('should handle IS_AUTHENTICATED false', () => {
    expect(isAuthenticated(undefined, {
      type: 'IS_AUTHENTICATED',
      isAuthenticated: false
    })).toMatchSnapshot()
  })
})
