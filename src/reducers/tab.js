// @flow
type State = number;
type Action = {type: string, tabId: number};

export const tab = (state:State = 2, action:Action) => {
  switch (action.type) {
    case 'CHANGE_TAB':
      if (action.tabId < 5 && action.tabId >= 0) {
        return action.tabId
      } else {
        return state
      }
    default:
      return state
  }
}
