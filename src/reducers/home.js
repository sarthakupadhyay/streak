// @flow
type Action = {type: string, toastId: number}
type State = number

export const toastId = (state: State = 0, action: Action) : State => {
  switch (action.type) {
    case 'SET_TOAST_ID':
      return action.toastId
    default:
      return state
  }
}
