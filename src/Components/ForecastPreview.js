// @flow
import React, { Component } from 'react'
import '../assets/stylesheets/ForecastPreview.css'
import moment from 'moment'
import _ from 'lodash'

type DayProps = {
  day: string,
  count: number
};

const Day = ({day, count}: DayProps) => (
  <div className='day'>
    <div className='day-name'>{day}</div>
    <div className='count'>{count}</div>
  </div>
)

type DaysProps = {
  days: Array<DayProps>
};

const Days = ({days}: DaysProps) => (
  <div className='days'>
    {days.map((day, i) =>
      <Day day={day.day} count={day.count} key={i} />
    )}
  </div>
)

type Props = {
  // counts: Array<number>
};

class ForecastPreview extends Component<Props> {
  getDays = () => {
    let days = ['Past', 'Today']
    for (let i = 1; i < 5; i++) {
      days.push(moment().add(i, 'days').format('ddd'))
    }
    days.push('Future')
    return days
  }

  render () {
    const days = _.zip(this.getDays(), [0, 0, 3, 0, 0, 42, 4]).map((z) =>
      ({ day: z[0], count: z[1] })
    )
    return (
      <div id='forecast-view'>
        <Days days={days} />
      </div>
    )
  }
}

export default ForecastPreview
