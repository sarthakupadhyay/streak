// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { changeTab } from '../actions/tab'
import ForecastPreview from './ForecastPreview'

type Props = {
  changeTab: (number) => void
};

class Dash extends Component<Props> {
  componentDidMount () {
    this.props.changeTab(2)
  }

  render () {
    return (
      <ForecastPreview />
    )
  }
}

export default connect(
  null,
  {
    changeTab: changeTab
  }
)(Dash)
