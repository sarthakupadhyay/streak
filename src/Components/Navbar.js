// @flow
import React, { Component } from 'react'
import '../assets/stylesheets/Navbar.css'
import {
  Link
} from 'react-router-dom'
import { connect } from 'react-redux'
import { sortTasks } from '../actions/tasks'
import _ from 'lodash'
import type { Task } from './Task/types/task_types.js'

type Props = {
  onChange: (string) => void,
  sortTasks: (string, string) => void,
  edit: boolean,
  pid: string,
  tasks: Array<Task>,
  projectTitle: string
};

type State = {
  val: string,
  sortOrder: string,
  sortKey: string,
  options: boolean,
  projectTitle: string,
  remaining: number
};

type NavbarHeaderProps = {
  projectTitle: string,
  remaining: number
};

const NavbarHeader = ({projectTitle, remaining}: NavbarHeaderProps) => (
  <div className='nav-header'>
    <div className='title'>{ projectTitle }</div>
    <div className='task-num'>{ remaining }</div>
  </div>
)

type SortOptionsProps = {
  toggleSort: (string) => void,
  toggleOptions: () => void,
};

const SortOptions = ({ toggleSort, toggleOptions }: SortOptionsProps) => (
  <div className='actions'>
    <div className='by'>SORT BY:</div>
    <div className='action' onClick={() => toggleSort('title')}>
      <i className='fa fa-sort' />&nbsp;
      <span>Abc</span>
    </div>
    <div className='action' onClick={() => toggleSort('priority')}>
      <i className='fa fa-sort' />&nbsp;
      <span>Priority</span>
    </div>
    <div className='action' onClick={() => toggleSort('dueDate')}>
      <i className='fa fa-sort' />&nbsp;
      <span>Due</span>
    </div>
    <div className='action close' onClick={() => toggleOptions()}>
      <i className='fa fa-times' />&nbsp;
      <span>close</span>
    </div>
  </div>
)

type OptionsProps = {
  toggleSort: (string) => void,
  toggleOptions: () => void,
  type: string
};

const Options = ({ toggleSort, type, toggleOptions }: OptionsProps) => {
  switch (type) {
    case 'SORT':
      return <SortOptions toggleSort={toggleSort} toggleOptions={toggleOptions} />
    default:
      return null
  }
}

class Navbar extends Component<Props, State> {
  constructor () {
    super()
    this.state = {
      val: '',
      sortOrder: 'desc',
      sortKey: 'created_on',
      options: false,
      type: '',
      remaining: 0,
      projectTitle: ''
    }
  }

  handleChange = (e) => {
    this.setState({
      val: e.target.value
    }, () => this.props.onChange(this.state.val))
  }

  componentDidMount () {
    if (this.props.edit) {
      // $FlowFixMe: suppressing this error until we figure what is this
      document.querySelector('#create-box').focus()
    }

    const { sortOrder, sortKey } = this.state

    this.props.sortTasks(sortOrder, sortKey)
  }

  componentDidUpdate (prevProps, prevState) {
    if (!_.isEqual(prevState, this.state)) {
      this.props.sortTasks(this.state.sortKey, this.state.sortOrder)
    }
    if (!_.isEqual(prevProps, this.props)) {
      const len = this.props.tasks.length
      const title = this.props.projectTitle
      this.setState({remaining: len, projectTitle: title})
    }
  }

  toggleSort = (key) => {
    const { sortOrder, sortKey } = this.state

    if (sortKey === key) {
      this.setState({ sortOrder: sortOrder === 'asc' ? 'desc' : 'asc' })
    } else {
      this.setState({ sortOrder: 'asc', sortKey: key })
    }
  }

  toggleOptions = () => {
    this.setState({ options: false })
  }

  render () {
    return (
      <div className='navigation'>
        <div className='navigation-items'>
          { !this.props.edit
            ? <Link to={`${this.props.pid}/task/new`} id='new-task-icon' className='placeholder'><i className='fa fa-plus search-icon' /><span className='search-box'> Create Task</span></Link>
            : (<div>
              <i className='fa fa-arrow-left search-icon' />
              <input id='create-box' value={this.state.val} type='text' className='search-box' placeholder='Add a title to task' onChange={this.handleChange} />
            </div>)
          }
          <div className={`toggle-options ${this.state.options ? 'open' : 'close'}`} onClick={() => this.setState({options: !this.state.options})}>
            <i className='fa fa-sort expand' />
          </div>
          <div className={`toggle-options`}>
            <i className='fa fa-filter expand' />
          </div>
          <div className={`toggle-options`}>
            <i className='fa fa-pencil-alt expand' />
          </div>
        </div>
        { this.state.options
          ? <Options toggleSort={this.toggleSort} toggleOptions={this.toggleOptions} type={'SORT'} />
          : (!this.props.edit ? <NavbarHeader remaining={this.state.remaining} projectTitle={this.state.projectTitle} /> : null)
        }
      </div>
    )
  }
}

export default connect(
  (state) => ({
    tasks: state.tasks
  }),
  {
    sortTasks: sortTasks
  }
)(Navbar)
