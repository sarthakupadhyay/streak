// @flow
import React, { Component } from 'react'
import {Route} from 'react-router-dom'
import Header from '../Header'
import '../../assets/stylesheets/Labels.css'
import _ from 'lodash'
import { connect } from 'react-redux'
import { changeTitle } from '../../actions/title'
import { changeTab } from '../../actions/tab'
import { fetchLabels } from '../../actions/labels'
import CreateLabel from './CreateLabel'
import EmptyView from '../EmptyView'
type Task = {
  title: string,
  id: string
};

export type Label = {
  title: string,
  id: string,
  tasks?: Array<Task>
};

type Props = {
  changeTitle: (string) => void,
  changeTab: (number) => void,
  fetchLabels: () => void,
  labels: Array<Label>,
  notify: (string, string) => void
};

type LabelItemProps = { label: Label };
const LabelItem = ({label}: LabelItemProps) => (
  <li className='label-item'>{label.title}</li>
)

type LabelListProps = { labels: Array<Label> };
const LabelList = ({ labels }: LabelListProps) => (
  <ul>
    {labels.map((label, i) => <LabelItem key={i} label={label} />)}
  </ul>
)

class Labels extends Component<Props> {

  componentDidMount () {
    this.props.changeTitle('Labels | Streaker')
    this.props.changeTab(1)
    if (_.isEmpty(this.props.labels)) {
      this.props.fetchLabels()
    }
  }

  render () {
    const labels = this.props.labels
    return (
      <div>
        <Header url='/labels/new' element='Label' />

        <div className='label-list'>
          {
            labels.length === 0
              ? <EmptyView tabName={'Labels'} />
              : <LabelList labels={labels} />
          }
        </div>

        <Route path='/labels/new' render={() => (
            <CreateLabel notify={this.props.notify} />
        )} />
      </div>
    )
  }
}

export default connect(
  (state : {labels: Array<Label>}) => ({
    labels: state.labels
  }),
  {
    changeTitle: changeTitle,
    changeTab: changeTab,
    fetchLabels: fetchLabels
  }
)(Labels)
