// @flow
import React, { Component } from 'react'
import * as firebase from 'firebase'
import Loader from '../Loader'
import { Link, Redirect } from 'react-router-dom'

type State = {
  label: string,
  working: boolean,
  uid: string
};

type Props = {
  notify: (string,string) => void
};

class CreateLabel extends Component<Props, State> {
  active : boolean
  constructor () {
    super()
    this.state = {
      label: '',
      uid: firebase.auth().currentUser.uid,
      working: false,
    }
    this.active = true
  }

  createLabel = () => {
      const taskRef = firebase.database().ref(`/labels/${this.state.uid}`)
      if(this.state.label === '') {
        this.props.notify('Please check label title!', 'error')
      } else {
        taskRef.push({
          title: this.state.label
        }).then(() => {
          this.props.notify('Label Created', 'success')
          this.active = false
          this.setState({working: false})
        }).catch(() => {
          this.props.notify('Please check label title!', 'error')
          this.setState({working: false})
        })
      }
    }

  render () {
    const form = (
      <div>
        <input type='text' className='create-label-input' placeholder='Create label' value={this.state.label} onChange={(e: SyntheticInputEvent<>) => this.setState({label: e.target.value})} />
        <div onClick={this.createLabel} className='create-label-btn'>Save</div>
        <Link to='/labels' className='create-label-cancel'>Cancel</Link>
      </div>
    )

    return (
      <div>
        {this.active ? (this.state.working ? <Loader /> : form) : <Redirect from='/labels/new' to='/labels' /> }
      </div>
    )
  }
}

export default CreateLabel
