// @flow
import React, { Component } from 'react'
import Tasks from '../Task/Tasks'
import type {Match} from 'react-router-dom'
class LabelTasks extends Component<{match: Match}> {
  render () {
    return (
      <Tasks filter='LABEL' options={this.props.match.params} />
    )
  }
}

export default LabelTasks
