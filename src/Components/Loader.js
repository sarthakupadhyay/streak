// @flow
import React from 'react'
import '../assets/stylesheets/Loader.css'

const Loader = (props : {color?: string}) => (
  <div className='loader-container' style={{background: props.color || '#FFF'}}>
    <div className='dummy-nav' />
    <div className='loader'>
      <div className='line' />
      <div className='line' />
      <div className='line' />
      <div className='line' />
    </div>
  </div>
)

export default Loader
