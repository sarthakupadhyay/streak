// @flow
import React, { Component } from 'react'
import Tasks from './Task/Tasks'
import { connect } from 'react-redux'
import { changeTitle } from '../actions/title'
import { changeTab } from '../actions/tab'

class Inbox extends Component<{changeTitle: (string) => void, changeTab: () => void}> {
  render () {
    return (
      <Tasks filter='INBOX' />
    )
  }
}

export default connect(
  null,
  {
    changeTitle: changeTitle,
    changeTab: changeTab
  }
)(Inbox)
