import React from 'react'
import ForecastPreview from '../ForecastPreview'
import { shallow } from 'enzyme'
const forecastPrev = shallow(<ForecastPreview />)

describe('<ForecastPreview />', () => {
  it('renders without crashing', () => {
    expect(forecastPrev).toHaveLength(1)
  })

  it('has one child', () => {
    expect(forecastPrev.children()).toHaveLength(1)
  })

  it('has child <Days />', () => {
    expect(forecastPrev.children().first().name()).toMatchSnapshot()
  })
})

describe('<Days />', () => {
  const days = forecastPrev.children().first().dive()
  it('renders without crashing', () => {
    expect(days).toHaveLength(1)
  })

  it('has class `days`', () => {
    expect(days.hasClass('days')).toEqual(true)
  })

  it('has 7 children', () => {
    expect(days.children()).toHaveLength(7)
  })
})

describe('<Day />', () => {
  const day = forecastPrev.children().first().dive()
  it('all days have class `day` ', () => {
    expect(day.children().map(n => n.dive().hasClass('day'))).toMatchSnapshot()
  })

  it('all days have props `day` and  `count`', () => {
    expect(day.children().map(n => Object.keys(n.props()))).toMatchSnapshot()
  })

  it('all days props not undefined', () => {
    day.children().forEach(n => {
      expect(n.props().count).not.toBeUndefined()
      expect(n.props().day).not.toBeUndefined()
    })
  })

  it('first day is `Past`', () => {
    expect(day.children().first().props().day).toEqual('Past')
  })

  it('last day is `Future`', () => {
    expect(day.children().last().props().day).toEqual('Future')
  })
})
