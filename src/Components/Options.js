// @flow
import React, { Component } from 'react'
import '../assets/stylesheets/Options.css'

export type Option = {
  label: string,
  action: () => void
}

const OptionItem = (props: Option) => (
  <div className='option' onClick={props.action}>
    {props.label}
  </div>
)

const OptionItems = (props) => (
  props.options.map((option: Option, i:number) =>
    <OptionItem label={option.label} action={option.action} key={i} />
  )
)

type State = {
  open: boolean,
  options: Array<Option>
}

type Props = {
  open: boolean,
  options?: Array<Option>,
  close: () => void
}

class Options extends Component<Props, State> {
  constructor () {
    super()
    this.state = {
      options: [
        {
          label: 'Close',
          action: this.close
        }
      ],
      open: false
    }
  }

  close = () => {
    this.setState({open: false})
    this.props.close()
  }

  componentDidMount () {
    if (this.props.options) {
      this.setState({
        options: this.props.options.concat(this.state.options)
      })
    }
    if (this.props.open) {
      this.setState({ open: true })
    }
  }

  render () {
    return (
      <div className='options'>
        {this.state.open ? <OptionItems options={this.state.options} /> : ''}
      </div>
    )
  }
}

export default Options
