// @flow
import React, { Component } from 'react'
import Tabbar from './Tabbar'
import Dash from './Dash'
import Projects from './Project/Projects'
import Labels from './Label/Labels'
import Loader from './Loader'
import Inbox from './Inbox'
import ProjectTasks from './Project/ProjectTasks'
import LabelTasks from './Label/LabelTasks'
import CreateTask from './Task/CreateTask'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify'
import * as firebase from 'firebase'
import { connect } from 'react-redux'
import { initTab } from '../actions/tab'
import { setToastId } from '../actions/home'

type Props = {
  initTab: (tabName: string) => void,
  setToastId: (id: number) => void,
  isComponentLoading: (isloading: bool) => void
};

type State = {
  create: boolean,
  toastId?: number | null,
  uid: string,
  labels?: {}
};

const Routes = ({components, methods}) => (
  components.map((component, i) =>
    <Route exact={component.exact || false} path={component.url} key={i}
      // $FlowFixMe: suppressing this error
      render={(props) => <component.component
        {...props}
        notify={methods.notify}
      />
      }
    />
  )
)

class Home extends Component<Props, State> {
  constructor () {
    super()
    this.state = {
      create: false,
      toastId: null,
      uid: firebase.auth().currentUser.uid
    }
  }

  componentDidMount = () => {
    this.props.initTab(window.location.href.split('/')[3] || '')
  }

  notify = (message:string, type:string) => {
    const position = {position: toast.POSITION.BOTTOM_LEFT}
    const { setToastId } = this.props
    switch (type) {
      case 'error':
        setToastId(toast.error(message, position))
        break
      case 'success':
        setToastId(toast.success(message, position))
        break
      default:
        setToastId(toast(message, position))
        break
    }
  }

  render () {
    const methods = {
      notify: this.notify
    }
    const components = [
      { url: '/labels', component: Labels },
      { url: '/label/:labelId', component: LabelTasks },
      { url: '/inbox', component: Inbox, exact: true },
      { url: '/project/:projectId', component: ProjectTasks, exact: true },
      { url: '/projects', component: Projects },
      { url: '/', component: Dash, exact: true },
      { url: '/task/new', component: CreateTask },
      { url: '/inbox/task/new', component: CreateTask },
      { url: '/project/:projectId/task/new', component: CreateTask }
    ]

    return (
      <Router>
        <div>
          { !this.props.isComponentLoading
            ? (
              <Switch>
                <Routes methods={methods} components={components} />
              </Switch>
            ) : <Loader />
          }
          <Tabbar />
          <ToastContainer autoClose={3000} />
        </div>
      </Router>
    )
  }
}

export default connect(
  (state) => ({
    isComponentLoading: state.isComponentLoading
  }),
  {
    initTab: initTab,
    setToastId: setToastId
  }
)(Home)

// Todo Check connected state for tab
