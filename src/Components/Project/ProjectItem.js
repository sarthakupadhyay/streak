import React, {Component} from 'react'
import Options from '../Options'
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
  arrayMove
} from 'react-sortable-hoc'
import { Link } from 'react-router-dom'
import type { Project } from './Projects'
import type {Option} from '../Options'

const DragHandle = SortableHandle(() => <span>:: &nbsp;</span>)

const SortableItem = SortableElement(({value, open}) =>
  <div>
    <div className='project-wrapper'>
      <div className='project'>
        <Link to={`/project/${value.id}`} style={{display: 'flex', flex: '10'}}>
          <DragHandle />
          <span className='icon' style={{background: value.meta.color || '#37D67A'}} />
          <span />
          <div className='project-title'>
            {value.meta.title}
          </div>
        </Link>
        <div className='menu' onClick={() => open(value.meta)}><i className='fa fa-ellipsis-v' /></div>
      </div>
      <div className='separator' />
    </div>
  </div>
)

const SortableList = SortableContainer(({items, open}) => {
  return (
    <div>
      {
        items.map((value, index) => (
          <SortableItem key={`item-${index}`} index={index} value={value} open={open} />
        ))
      }
    </div>
  )
})

type State = {
  options: boolean,
  optionItems?: Array<Option>,
};

type Props = {
  projects: Array<Project>,
  archive: (Project) => void
};

class ProjectItem extends Component<Props, State> {
  constructor () {
    super()
    this.state = {
      items: [],
      options: false
    }
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    this.setState({
      items: arrayMove(this.state.items, oldIndex, newIndex)
    })
  }

  openOptions = (project: Project) => {
    this.setState({
      options: true,
      optionItems: [
        {
          label: 'Archive Project',
          action: () => this.props.archive(project)
        }
      ]
    })
  }

  render () {
    return (
      <div>
        {this.state.options
          ? <Options
            open={this.state.options}
            close={() => this.setState({options: false})}
            options={this.state.optionItems}
          />
          : ''
        }
        <SortableList items={this.props.projects} onSortEnd={this.onSortEnd} useDragHandle open={this.openOptions} />
      </div>
    )
  }
}

export default ProjectItem
