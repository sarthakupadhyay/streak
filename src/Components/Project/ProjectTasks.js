// @flow
import React, { Component } from 'react'
import Tasks from '../Task/Tasks'
import _ from 'lodash'
import * as firebase from 'firebase'
// import type { Match } from 'react-router-dom'

type State = {
  projectMeta: {
    id?: string,
    color?: string,
    title?: string
  }
};

type Props = {
  match: {
    params: {
      projectId: string
    }
  }
};

class ProjectTasks extends Component<Props, State> {
  constructor () {
    super()
    this.state = { projectMeta: {} }
  }
  componentDidMount () {
    const uid = firebase.auth().currentUser.uid
    const pid = this.props.match.params.projectId

    firebase.database().ref(`/projects/${uid}/${pid}/meta`).once('value').then((snap) => {
      this.setState({ projectMeta: _.set(snap.val(), 'id', pid) })
    })
  }

  render () {
    return (
      <Tasks filter='PROJECT' options={this.props.match.params} projectMeta={this.state.projectMeta} />
    )
  }
}

export default ProjectTasks
