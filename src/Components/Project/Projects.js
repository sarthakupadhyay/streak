// @flow
import React, { Component } from 'react'
import _ from 'lodash'
import '../../assets/stylesheets/Projects.css'
import Header from '../Header'
import CreateProject from './CreateProject'
import ProjectItem from './ProjectItem'
import * as firebase from 'firebase'
import { Route } from 'react-router-dom'
import { connect } from 'react-redux'
import { changeTitle } from '../../actions/title'
import { changeTab } from '../../actions/tab'
import { fetchProjects } from '../../actions/projects'
import EmptyView from '../EmptyView'

export type Project = {
  id: string,
  title: string,
  color: string,
  tasks?: {}
};

type Props = {
  changeTitle: (string) => void,
  changeTab: (number) => void,
  fetchProjects: () => void,
  projects: Array<Project>,
  notify: () => void
};

type State = {
  uid: string
};

class Projects extends Component<Props, State> {
  constructor () {
    super()
    this.state = {
      uid: firebase.auth().currentUser.uid
    }
  }

  archiveProject = (project : Project) => {
    const taskRef = firebase.database().ref(`/archivedProjects/${this.state.uid}/${project.id}`)
    taskRef.set(project)
  }

  componentDidMount () {
    this.props.changeTitle('Projects | Streaker')
    this.props.changeTab(0)
    if (_.isEmpty(this.props.projects)) {
      this.props.fetchProjects()
    }
  }

  render () {
    const { projects } = this.props
    return (
      <div className='projects'>
        <div>
          <Header url='/projects/new' element='Project' />
          <div className='project-list'>
            {
              projects.length === 0
                ? <EmptyView tabName={'Projects'} />
                : <ProjectItem archive={this.archiveProject} projects={this.props.projects} />
            }
          </div>
          <Route path='/projects/new' render={() => (
            <CreateProject notify={this.props.notify} />
          )} />
        </div>
      </div>
    )
  }
}

export default connect(
  (state : {projects: Array<Project>}) => ({
    projects: state.projects
  }),
  {
    changeTitle: changeTitle,
    changeTab: changeTab,
    fetchProjects: fetchProjects
  }
)(Projects)
