// @flow
import React, { Component } from 'react'
import * as firebase from 'firebase'
import { BlockPicker } from 'react-color'
import Loader from '../Loader'
import {
  Link, Redirect
} from 'react-router-dom'

type State = {
  projectName: string,
  projectColor: string,
  working: boolean,
  colorPickerShow: boolean
};

type Props = {
  notify: (string,string) => void
};

class CreateProject extends Component<Props, State> {
  active : boolean
  constructor () {
    super()
    this.state = {
      projectName: '',
      projectColor: '#37D67A',
      working: false,
      colorPickerShow: false
    }
    this.active = true
  }

  saveProject = () => {
    const uid = firebase.auth().currentUser.uid
    const projRef = firebase.database().ref(`/projects/${uid}/`)
    if(this.state.projectName === '') {
      this.props.notify('Please check project title!', 'error')
    } else {
      this.setState({working: true})
      projRef.push({meta: {
            title: this.state.projectName,
            color: this.state.projectColor
        }}).then(() => {
          this.toggleColorPicker()
          this.props.notify('Project Created', 'success')
          this.active = false
          this.setState({working: false})
        }).catch(() => {
          this.props.notify('Please check project title!', 'error')
          this.setState({working: false})
        })
      }
    }

  toggleColorPicker = () => {
    this.setState({
      colorPickerShow: !this.state.colorPickerShow
    })
  }

  handleChange = (e: SyntheticInputEvent<>) => {
    this.setState({
      projectName: e.target.value
    })
  }

  render () {
    const form = (
      <div className='create-project'>
        <div className='create-project-field'>
          <span className='project-color' onClick={this.toggleColorPicker} style={{background: this.state.projectColor}} />
          <input type='text' placeholder='New Project' value={this.state.projectName} onChange={this.handleChange} />
        </div>
        <div onClick={this.saveProject} className='create-project-btn'>Save</div>
        <Link to='/projects' className='create-project-cancel'>Cancel</Link>
        <br /><br />
        {this.state.colorPickerShow ? <BlockPicker triangle='hide' onChange={color => this.setState({projectColor: color.hex})} /> : ''}
      </div>
    )

    return (
      <div>
        {this.active ? (this.state.working ? <Loader /> : form) : <Redirect from='/projects/new' to='/projects' /> }
      </div>
    )
  }
}

export default CreateProject
