// @flow
import React from 'react'

const EmptyView = (props: {tabName:string}) => (
  <div style={{color: '#CCC', fontSize: '20px', textAlign: 'center'}}>
    No {props.tabName}
  </div>
)

export default EmptyView
