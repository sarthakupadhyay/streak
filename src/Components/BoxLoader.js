// @flow
import React from 'react'
import '../assets/stylesheets/BoxLoader.css'

const BoxLoader = () => (
  <div className='box-loader-wrapper'>
    <div className='box-loader container'>
      <div className='holder'>
        <div className='box' />
      </div>
      <div className='holder'>
        <div className='box' />
      </div>
      <div className='holder'>
        <div className='box' />
      </div>
    </div>
  </div>
)

export default BoxLoader
