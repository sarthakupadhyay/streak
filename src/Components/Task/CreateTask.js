// @flow
import React, { Component } from 'react'
import * as firebase from 'firebase'
import '../../assets/stylesheets/CreateTask.css'
import _ from 'lodash'
import InputMoment from 'input-moment'
import Select from 'react-select'
// $FlowFixMe: suppressing this error until we add libdef
import 'input-moment/dist/input-moment.css'
import TextareaAutosize from 'react-autosize-textarea'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { changeTitle } from '../../actions/title'
import { changeTab } from '../../actions/tab'
import { fetchProjects } from '../../actions/projects'
import { fetchLabels } from '../../actions/labels'
import { fetchTasks } from '../../actions/tasks'
import Navbar from '../Navbar'
import type { Project } from '../Project/Projects'
// import type { Task } from './Tasks'
import type { Label } from '../Label/Labels'

type PlaceholderProps = {
  label: string,
  value: string,
  onClick?: () => void
};

type CommonLabelValueType = {
  label: string,
  value: string
};

const Placeholder = ({label, value, onClick}: PlaceholderProps) => (
  <div className={`placeholder ${value}`} onClick={onClick}>
    <span>{label}</span>
    <span>{value}</span>
  </div>
)

type Props = {
  notify: (string, string) => void,
  changeTitle: (string) => void,
  fetchProjects: () => void,
  fetchLabels: () => void,
  projects: Array<Project>,
  labels: Array<Label>,
  match: {
     params: {
       projectId: any,
     }
   }
};

type State = {
  title?: string,
  project: {label?: string, value?: string, color?: string},
  repeat?: string,
  note?: string,
  priority?: {label: string, value: string},
  label: Array<{label: string, value: string}>,
  active?: string,
  deferDate?: string | null,
  dueDate?: string | null,
  selectedOption?: {label: string, value:string},
  value?: string,
  momentDue: any,
  momentDefer: any
};

class CreateTask extends Component<Props, State> {
  constructor () {
    super()
    this.state = {
      active: '',
      momentDue: moment(),
      momentDefer: moment(),
      label: [],
      project: {},
      title: ''
    }
  }

  toggleField = (field) => {
    console.log(field)
    this.setState({ active: field })
  }

  submit = () => {
    const uid = firebase.auth().currentUser.uid
    let taskRef = firebase.database().ref(`/inbox/${uid}/tasks/`)
    if (this.state.title === '') {
      this.props.notify('Error while creating task! Please try again', 'error')
    } else {
      if (!_.isEmpty(this.state.project)) {
        // $FlowFixMe: suppressing this error
        taskRef = firebase.database().ref(`/projects/${uid}/${this.state.project.value}/tasks/`)
      }
      taskRef.push({
        title: this.state.title,
        labels: this.state.label.map(label => ({title: label.label, id: label.value})) || null,
        note: this.state.note || null,
        priority: this.state.priority ? this.state.priority.value : null,
        repeat: this.state.repeat || null,
        dueDate: this.state.dueDate || null,
        deferDate: this.state.deferDate || null
      }).then(() => {
        // this.props.history.goBack()
      }).catch(() => {
        // this.props.notify('Error while creating task! Please try again', 'error')
      })
    }
  }

  handleChange (selectedOption) {
    this.setState({ selectedOption: selectedOption })
    this.setState({ value: selectedOption.value })
  }

  componentDidMount () {
    this.props.changeTitle('Create Task | Streaker')
    if (_.isEmpty(this.props.projects)) {
      this.props.fetchProjects()
    }
    if (_.isEmpty(this.props.projects)) {
      this.props.fetchLabels()
    }
  }

  mapProjectsForOptions = (projectsFromProps) => (
    projectsFromProps.map(project => ({
      // $FlowFixMe: suppressing this error
      label: project.meta.title,
      value: project.id,
      // $FlowFixMe: suppressing this error
      color: project.meta.color
    }))
  )

  render () {
    const projectId = this.props.match.params.projectId
    let backUrl = '/inbox'
    let projectOptions = []
    if (typeof projectId === 'undefined') {
      projectOptions = this.mapProjectsForOptions(this.props.projects)
    } else {
      backUrl = `/project/${this.props.match.params.projectId}`
      let filteredProject = this.props.projects.filter(project => (project.id === projectId))
      projectOptions = this.mapProjectsForOptions(filteredProject)
      if (_.isEmpty(this.state.project)) {
        this.setState({project: projectOptions[0]})
      }
    }

    const labelOptions = this.props.labels.map((label) => (
      {
        label: label.title,
        value: label.id
      })
    )

    const priorities: Array<CommonLabelValueType> = [
      {label: 'Priority 1', value: 'p1'},
      {label: 'Priority 2', value: 'p2'},
      {label: 'Priority 3', value: 'p3'}
    ]

    const reapeatOpts: Array<CommonLabelValueType> = [
      {label: 'Every Day', value: 'd'},
      {label: 'Evrery other day', value: 'd2'},
      {label: 'Every Week', value: 'w'},
      {label: 'Every Month', value: 'm'}
    ]

    return (
      <div>
        <Navbar edit onChange={(title) => this.setState({ title })} pid='' />
        <div className='create-task'>
          <div className='add-project'>
            <div className='section'>
              { this.state.active === 'project'
                ? <Select
                  value={this.state.project}
                  options={projectOptions}
                  placeholder='Select a project'
                  autoFocus
                  onChange={value => this.setState({ project: value })}
                />
                : <Placeholder label='Project' value={this.state.project.label || 'none'} onClick={() => this.toggleField('project')} />
              }
            </div>
            <div className='section'>
              { this.state.active === 'due'
                ? <div>
                  <div>
                    <button onClick={() => this.setState({ dueDate: null }, () => { this.toggleField('') })}>Clear</button>
                    <button onClick={() => this.setState({ momentDue: moment().add(24, 'hours'), dueDate: moment().add(24, 'hours').format() }, () => { this.toggleField('') })}>Tommorow</button>
                    <button onClick={() => this.setState({ momentDue: moment().add(7, 'days'), dueDate: moment().add(7, 'days').format() }, () => { this.toggleField('') })}>NextWeek</button>
                    <button onClick={() => this.setState({ momentDue: moment().add(Math.floor(Math.random() * 21 + 10), 'days'), dueDate: moment().add(Math.floor(Math.random() * 21 + 10), 'days').format() }, () => { this.toggleField('') })}>Random</button>
                  </div>
                  <InputMoment
                    moment={this.state.momentDue}
                    onChange={(m) => this.setState({ momentDue: m, dueDate: m.format() })}
                    minStep={5}
                    prevMonthIcon='fa fa-angle-left'
                    nextMonthIcon='fa fa-angle-right'
                    onSave={() => this.toggleField('')}
                  />
                </div>
                : <Placeholder onClick={() => this.toggleField('due')} label='Due' value={this.state.dueDate ? moment(this.state.dueDate).calendar() : 'none'} />
              }
            </div>
            <div className='section'>
              { this.state.active === 'defer'
                ? <div>
                  <div>
                    <button onClick={() => this.setState({ deferDate: null }, () => { this.toggleField('') })}>Clear</button>
                    <button onClick={() => this.setState({ momentDefer: moment().add(24, 'hours'), deferDate: moment().add(24, 'hours').format() }, () => { this.toggleField('') })}>Tommorow</button>
                    <button onClick={() => this.setState({ momentDefer: moment().add(7, 'days'), deferDate: moment().add(7, 'days').format() }, () => { this.toggleField('') })}>NextWeek</button>
                    <button onClick={() => this.setState({ momentDefer: moment().add(Math.floor(Math.random() * 21 + 10), 'days'), deferDate: moment().add(Math.floor(Math.random() * 21 + 10), 'days').format() }, () => { this.toggleField('') })}>Random</button>
                  </div>
                  <InputMoment
                    moment={this.state.momentDefer}
                    onChange={(m) => this.setState({ momentDefer: m, deferDate: m.format() })}
                    minStep={5}
                    prevMonthIcon='fa fa-angle-left'
                    nextMonthIcon='fa fa-angle-right'
                    onSave={() => this.toggleField('')}
                  />
                </div>
                : <Placeholder onClick={() => this.toggleField('defer')} label='Defer' value={this.state.deferDate ? moment(this.state.deferDate).calendar() : 'none'} />
              }
            </div>
            <div className='section'>
              { this.state.active === 'label'
                ? <Select
                  value={this.state.label}
                  options={labelOptions}
                  placeholder='Set a Label'
                  autoFocus
                  isMulti
                  onChange={value => this.setState({ label: value })}
                />
                : <Placeholder label='Label' value={this.state.label.map(label => label.label).join(', ') || 'none'} onClick={() => this.toggleField('label')} />
              }
            </div>
            <div className='section priority'>
              { this.state.active === 'priority'
                ? <Select
                  value={this.state.priority}
                  options={priorities}
                  placeholder='Set a Priority'
                  autoFocus
                  onChange={value => this.setState({ priority: value })}
                />
                : <Placeholder label='Priority' value={this.state.priority ? this.state.priority.label : 'none'} onClick={() => this.toggleField('priority')} />
              }
            </div>
            <div className='section'>
              { this.state.active === 'note'
                ? <div>
                  <TextareaAutosize
                    maxRows={15}
                    autoFocus
                    style={{minHeight: 100}}
                    placeholder='Add a note'
                    value={this.state.note}
                    onChange={e => this.setState({ note: e.target.value })}
                  />
                  <div>
                    <button onClick={() => { this.setState({active: ''}) }}>Done</button>
                    <button onClick={() => { this.setState({note: ''}, () => { this.toggleField('') }) }}>Cancel</button>
                    <button onClick={() => { this.setState({note: ''}) }}>Reset</button>
                  </div>
                </div>
                : <Placeholder label='Note' value={this.state.note ? 'Expand note' : 'none'} onClick={() => this.toggleField('note')} />
              }
            </div>
            <div className='section' onClick={() => this.toggleField('')}>
              { this.state.active === 'repeat'
                ? <Select
                  value={this.state.repeat}
                  options={reapeatOpts}
                  placeholder='Repeat'
                  onChange={value => this.setState({ repeat: value })}
                />
                : <Placeholder label='Repeat' value={'none'} />
              }
            </div>
            <div className='section'>
              { this.state.active === 'flag'
                ? <Select
                  value={this.state.repeat}
                  options={reapeatOpts}
                  placeholder='Repeat'
                  onChange={value => this.setState({ repeat: value })}
                />
                : <Placeholder label='Flag' value={'none'} />
              }

            </div>
          </div>
          <div className='buttons'>
            <Link to={backUrl} className='discard-btn btn'>Discard</Link>
            <Link to={backUrl + '/task/new'} className='save-btn btn' onClick={this.submit}>Save +</Link>
            <Link to={backUrl} className='save-btn btn' onClick={this.submit}>Save</Link>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    projects: state.projects,
    labels: state.labels,
    tasks: state.tasks
  }),
  {
    changeTitle: changeTitle,
    changeTab: changeTab,
    fetchProjects: fetchProjects,
    fetchLabels: fetchLabels,
    fetchTasks: fetchTasks
  }
)(CreateTask)
