// @flow
import React, {Component} from 'react'
import '../../assets/stylesheets/Tasks.css'
import TaskItem from './TaskItem'
import Navbar from '../Navbar'
import { connect } from 'react-redux'
import { changeTitle } from '../../actions/title'
import { changeTab } from '../../actions/tab'
import { fetchTasks, stopFetch } from '../../actions/tasks'
import EmptyView from '../EmptyView'
import type { Props, State } from './types/task_types.js'

const TaskList = ({tasks, projectMeta}) => (
  tasks.map((task, i) =>
    <TaskItem task={task} key={i} projectMeta={projectMeta} />
  )
)

class Tasks extends Component<Props, State> {
  componentDidMount () {
    switch (this.props.filter) {
      case 'INBOX':
        this.props.changeTitle('Inbox | Streaker')
        this.props.changeTab(3)
        this.props.fetchTasks(this.props.filter)
        break
      case 'ALL':
        this.props.changeTitle('Tasks | Streaker')
        this.props.changeTab(2)
        this.props.fetchTasks()
        break
      case 'PROJECT':
        this.props.changeTitle('Tasks | Streaker')
        this.props.changeTab(0)
        this.props.fetchTasks('PROJECT', this.props.options)
        break
      case 'LABEL':
        this.props.changeTitle('Tasks | Streaker')
        this.props.changeTab(0)
        this.props.fetchTasks('LABEL', this.props.options)
        break
      default:
        this.props.changeTitle('Tasks | Streaker')
        this.props.fetchTasks()
        this.props.changeTab(2)
        break
    }
  }

  componentWillUnmount () {
    this.props.stopFetch(this.props.filter)
  }

  updateTitle = (title:string) => {
    this.setState({
      title: title
    })
  }

  render () {
    const { tasks } = this.props
    // console.log(tasks)
    const pid = typeof this.props.options === 'undefined' ? '' : this.props.options.projectId
    const projectTitle = typeof this.props.projectMeta === 'undefined' ? 'Inbox' : this.props.projectMeta.title
    return (
      <div>
        <Navbar edit={false} pid={pid} onChange={(s) => console.log()} projectTitle={projectTitle}/>
        <div className='task-list'>
          {
            tasks.length === 0
              ? <EmptyView tabName={'Tasks'} />
              : <TaskList tasks={tasks} projectMeta={this.props.projectMeta} />
          }
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    tasks: state.tasks
  }),
  {
    changeTitle: changeTitle,
    changeTab: changeTab,
    fetchTasks: fetchTasks,
    stopFetch: stopFetch
  }
)(Tasks)
