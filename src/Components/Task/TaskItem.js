// @flow
import React, {Component} from 'react'
import * as firebase from 'firebase'
import type { Task, Project, Label } from './types/task_types.js'
import moment from 'moment'
import _ from 'lodash'
import { Link } from 'react-router-dom'

type Props = {
  task: Task,
  projectMeta: {
    id: string,
    color: string,
    title: string
  }
};

type State = {
  uid: string
};

type ProjectProps = {
  project: Project
};

const ProjectLabel = ({project} : ProjectProps) => (
  <div>
    <Link to={`/project/${project.id}`} className='project-name'>
      <div className='color-circle' style={{'background': project.color}} />
      {project.title}
    </Link>
  </div>
)

type PriorityMarkerProps = {
  priority: string
};

const PriorityMarker = ({priority}: PriorityMarkerProps) => (
  <div className={`priority ${priority}`} />
)

type TaskLabelProps = {
  label: Label
};

const TaskLabel = ({label}: TaskLabelProps) => (
  <Link to={`/label/${label.id}`} className='label' >@{label.title}</Link>
)

const TaskLabels = ({labels}) => (
  labels.map((label, i) => <TaskLabel label={label} key={i} />)
)

class TaskItem extends Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      uid: firebase.auth().currentUser.uid
    }
  }

  markDone = () => {
    let taskRef
    if (!_.isEmpty(this.props.projectMeta)) {
      taskRef = firebase.database().ref(`/projects/${this.state.uid}/${this.props.projectMeta.id}/tasks/${this.props.task.id}/done`)
    } else {
      taskRef = firebase.database().ref(`/inbox/${this.state.uid}/tasks/${this.props.task.id}/done`)
    }
    taskRef.set(true).then(snap => { /* success */ })
  }

  markUnDone = () => {
    let taskRef
    if (!_.isEmpty(this.props.projectMeta)) {
      taskRef = firebase.database().ref(`/projects/${this.state.uid}/${this.props.projectMeta.id}/tasks/${this.props.task.id}/done`)
    } else {
      taskRef = firebase.database().ref(`/inbox/${this.state.uid}/tasks/${this.props.task.id}/done`)
    }
    taskRef.set(false).then(snap => { /* success */ })
  }

  render () {
    const task = this.props.task
    const { labels, dueDate, deferDate } = task
    const project = this.props.projectMeta
    let future = false
    if (deferDate) {
      future = new Date(deferDate) > new Date()
    }

    return (
      <div className={`task${task.done ? ' done' : ''}${project ? ' project' : ''}${future ? ' future' : ''}`}>
        <PriorityMarker priority={task.priority || ''} />
        <div className='main'>
          <div className='info'>
            <div className='title'>
              {task.title}
            </div>
            <div className='more'>
              <div className='project'>
                {project ? <ProjectLabel project={project} /> : 'Inbox'}
              </div>
              <div className='labels'>
                {labels ? <TaskLabels labels={labels} /> : null}
              </div>
              <div className='time'>
                {dueDate ? moment(task.dueDate).calendar() : null}
              </div>
            </div>
          </div>
          <div className='status' onClick={!task.done ? this.markDone : this.markUnDone}>
            <i className='fa fa-check' />
          </div>
        </div>
        <div className='separator' />
      </div>
    )
  }
}

export default TaskItem
