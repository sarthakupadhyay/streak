export type Project = {
  id: string,
  title: string,
  color: string
};

export type Label ={
  id: string,
  title: string
};

export type Task = {
  title: string,
  done?: string,
  created_on: number,
  id: string,
  project?: Project,
  labels?: Array<Label>,
  priority?: string,
  dueDate?: string,
  deferDate?: string
};

export type Props = {
  filter: string,
  tasks: Array<Task>,
  fetchTasks: (filter: ?string, options: ?{}) => void,
  stopFetch: (filter: ?string) => void,
  changeTitle: (string) => void,
  changeTab: (number) => void,
  options?: {projectId: string},
  projectMeta: {
    id: string,
    color: string,
    title: string
  }
};

export type State = {
  title?: string,
  create: boolean
};
