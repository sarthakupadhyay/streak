// @flow
import React, { Component } from 'react'
import '../assets/stylesheets/Tabbar.css'
import {Link} from 'react-router-dom'
import { connect } from 'react-redux'

type Data = {
  url: string,
  active: string,
  icon: string,
  label: string
};

type TabProps = {
  data: Data,
  id: number
};

const Tab = ({data, id}: TabProps) => (
  <Link to={data.url} className={`tab-element ${data.active}`} data-id={id}>
    <i className={`fa icon ${data.icon}`} data-id={id} />
    <span data-id={id} className='label'>{data.label}</span>
  </Link>
)

const Tabs = ({tabData}) => (
  tabData.map((data: Data, i: number) =>
    <Tab data={data} id={i} key={i} />
  )
)

type Props = {
  tab: number
};

type State = {
  tabData: Array<Data>
};

class Tabbar extends Component<Props, State> {
  constructor () {
    super()
    this.state = { tabData: [
      { icon: 'fa-book', label: 'Projects', url: '/projects', active: '' },
      { icon: 'fa-tags', label: 'Labels', url: '/labels', active: '' },
      { icon: 'fa-home', label: 'Home', url: '/', active: '' },
      { icon: 'fa-inbox', label: 'Inbox', url: '/inbox', active: '' },
      { icon: 'fa-user', label: 'Account', url: '/', active: '' }
    ]}
  }

  updateSelectedTab(tabNumber) {
    this.setState((prevState) => ({
      tabData: prevState.tabData.map((data,i) => {
        data.active = i === tabNumber ? 'selected' : ''
        return data
      })
    }))
  }

  componentDidMount () {
    this.updateSelectedTab(this.props.tab)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.tab !== this.props.tab) {
      this.updateSelectedTab(nextProps.tab)
    }
  }

  render () {
    return (
      <div className='tab-bottom'>
        <Tabs tabData={this.state.tabData} />
      </div>
    )
  }
}

// $FlowFixMe: suppressing this error until we figure what this is
export default connect(
  (state) => ({
    tab: state.tab
  })
)(Tabbar)
