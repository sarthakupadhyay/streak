// @flow
import React from 'react'
import {
  Link
} from 'react-router-dom'
import '../assets/stylesheets/Header.css'

const Header = (props: {url:string, element: string}) => (
  <div className='simple-nav'>
    <span className='heading'>{props.element}s</span>
    <Link to={props.url} className='create-link'>
      <span className='create-icon'>+</span>
      <span>Create {props.element}</span>
    </Link>
  </div>
)

export default Header
