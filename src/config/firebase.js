// @flow
type fireConfig = {
  apiKey: string,
  authDomain: string,
  databaseURL: string,
  projectId: string,
  storageBucket: string,
  messagingSenderId: string
}
export const firebaseConfig: fireConfig = {
  apiKey: 'AIzaSyBci2C0cDtBKkCP41PhcaGJ4q2hIiedUkA',
  authDomain: 'streaker-42.firebaseapp.com',
  databaseURL: 'https://streaker-42.firebaseio.com',
  projectId: 'streaker-42',
  storageBucket: 'streaker-42.appspot.com',
  messagingSenderId: '774122592401'
}
