declare module "react-sortable-hoc" {

  declare export function SortableContainer(React$ComponentType<any>) : React$ComponentType<any>
  declare export function SortableElement(React$ComponentType<any>) : React$ComponentType<any>
  declare export function SortableHandle(React$ComponentType<any>) : React$ComponentType<any>
}
//   declare type SortableContainerProps = {
// //     axis: PropTypes.oneOf(['x', 'y', 'xy']),
// //       distance: PropTypes.number,
// //       lockAxis: PropTypes.string,
// //       helperClass: PropTypes.string,
// //       transitionDuration: PropTypes.number,
// //       contentWindow: PropTypes.any,
// //       onSortStart: PropTypes.func,
// //       onSortMove: PropTypes.func,
// //       onSortEnd: PropTypes.func,
// //       shouldCancelStart: PropTypes.func,
// //       pressDelay: PropTypes.number,
// //       useDragHandle: PropTypes.bool,
// //       useWindowAsScrollContainer: PropTypes.bool,
// //       hideSortableGhost: PropTypes.bool,
// //       lockToContainerEdges: PropTypes.bool,
// //       lockOffset: PropTypes.oneOfType([
// //         PropTypes.number,
// //         PropTypes.string,
// //         PropTypes.arrayOf(
// //           PropTypes.oneOfType([PropTypes.number, PropTypes.string])
// //         ),
// //       ]),
// //       getContainer: PropTypes.func,
// // getHelperDimensions: PropTypes.func,
//   }

//   declare type SortableElementProps = {

//   }

//   declare type SortableHandleProps = {

//   }
