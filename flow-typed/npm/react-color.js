declare module "react-color" {
  declare type Props = {
    triangle: string,
    onChange: (color: Color) => void
  }

  declare type Color = {
    hex: string
  }

  declare export class BlockPicker extends React$Component<Props> {}
}
