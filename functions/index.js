const functions = require('firebase-functions')
const admin = require('firebase-admin')
admin.initializeApp(functions.config().firebase)
const db = admin.database()

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.addTasksToProjects = functions.database.ref('/tasks/{userId}/{taskId}/').onCreate(event => {
  const task = event.data.val()
  const taskId = event.params.taskId
  const userId = event.params.userId
  const taskObject = {title: task.title, id: taskId}

  if (task.project) {
    const projectId = task.project.id
    const projRef = db.ref('/projects/' + userId + '/' + projectId + '/tasks/' + taskId)
    projRef.set(taskObject).then(() => {
      console.log(taskId + ' added to project with projectId: ' + projectId)
    }).catch((e) => {
      console.log('Error adding to project : ', e)
    })
  } else {
    const inboxRef = db.ref('/inbox/' + userId + '/tasks/' + taskId)
    inboxRef.set(taskObject).then(() => {
      console.log(taskId + ' added to inbox')
    }).catch((e) => {
      console.log('Error adding to inbox : ', e)
    })
  }
  console.log(task.labels)
  if (task.labels) {
    for (let i = 0; i < task.labels.length; i++) {
      admin.database().ref(`/labels/${userId}/${task.labels[i].id}/tasks/${taskId}`).set({
          id: taskId,
          title: task.title
      }).then(() => {
        console.log('Sucessfully added task to label')
      }).catch(() => {
        console.log('Failed adding task to label')
      })
    }
  }
})

exports.archiveProjects = functions.database.ref('/archivedProjects/{userId}/{projectId}/').onCreate(event => {
  const projectId = event.params.projectId
  const ref = db.ref('/projects/' + event.params.userId + '/' + projectId)
  db.ref('/archivedProjects/' + event.params.userId + '/' + projectId).once('value').then(function (snapshot) {
    Object.keys(snapshot.val().tasks).forEach( function(taskId, index) {
      db.ref('/tasks/' + event.params.userId + '/' + taskId).once('value').then(function(snap) {
        db.ref('/archivedTasks/' + event.params.userId + '/' + taskId ).set(snap.val()).then(function () {
          db.ref('/tasks/' + event.params.userId + '/' + taskId).remove()
        })
      })
    });
    ref.remove()
  })
})

exports.timestampTasks = functions.database.ref('/tasks/{userId}/{taskId}/').onCreate(event => {
  event.data.ref.child('created_on').set(Number(new Date()))
  event.data.ref.child('done').set(false)
  event.data.ref.child('id').set(event.params.taskId)
})

exports.archiveInboxTasks = functions.database.ref('/tasks/{userId}/{taskId}/done').onUpdate(event => {
  const taskId = event.params.taskId
  const userId = event.params.userId
  const taskRef = admin.database().ref(`/tasks/${userId}/${taskId}`)
  taskRef.once('value').then(snap => {
    console.log(snap.val());
    const task = snap.val()
    if (task.done && !task.project) {
      console.log('1')
      admin.database().ref(`/archivedTasks/${userId}/${taskId}`).set(task).then(() => {
        taskRef.remove()
        admin.database().ref(`/inbox/${userId}/tasks/${taskId}`).remove()
      }).catch(e => {
        console.log('failed', e)
      })
    } else {
      console.log('0')
      console.log(task.project.id)
      admin.database().ref(`/projects/${userId}/${task.project.id}/`)
        .child('modified_on').set(Number(new Date())).then(() => console.log('success')).catch(() => console.log('fakil'))
    }
  }).catch(e => {
    console.log('failed', e)
    return 400
  })
})

